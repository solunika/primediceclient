var dummyResponse ={
"bet": {
    "id": 9866821600,
    "player": "Gabenitez",
    "player_id": "1426964",
    "amount": 248,
    "target": 79.2,
    "profit": 62,
    "win": true,
    "condition": "<",
    "roll": 6.72,
    "nonce": 15151,
    "client": "b7042e41e5e32eb27f061d81aa7146b6",
    "multiplier": 1.25,
    "timestamp": "2016-03-25T20:47:09.031Z",
    "jackpot": false,
    "server": "268c2d1e03c26a7c899f7975a56ec57b0d928b8d64b82aa6646032fe34e98fca",
    "revealed": false
},
"user": {
    "id": 1,
    "userid": "1426900",
    "username": "Gabenitez",
    "balance": 2962400.0000,
    "password": true,
    "address": "1Lwg8khAHDeEadRy1obgKxuGYTysMMYqie",
    "registered": "2016-03-24T18:51:38.101Z",
    "otp_enabled": false,
    "email_enabled": false,
    "address_enabled": false,
    "wagered": 9244472,
    "profit": -806990.0169,
    "bets": 15152,
    "wins": 12046,
    "losses": 3106,
    "win_risk": 25016645,
    "lose_risk": 24520655,
    "messages": 0,
    "referred": 0,
    "affiliate_total": 0,
    "nonce": 15152,
    "client": "b7042e41e5e32eb27f061d81aa7146b6",
    "previous_server": null,
    "previous_client": null,
    "previous_server_hashed": null,
    "next_seed": "5d34b09a6d8a04ced7761abb89f448f97687151db49a4a033dac173122533374",
    "server": "268c2d1e03c26a7c899f7975a56ec57b0d928b8d64b82aa6646032fe34e98fca"
}};

function getUserInfo(callback) {
    callback(dummyResponse.user);
} 

module.exports = {
    dummyResponse: dummyResponse,
    getUserInfo: getUserInfo
}