
var lostInARow =0;
var bets = 0;
var waitForSafeStreak = false;
var lastStreak = {};
var safeStreak = 7;
var statustxt = "Profit/Lose = xxx";
var userbal = $("#lblPlayerBalance");
var txtPayout = $("#txtPayoutRate");
var txtChance = $("#txtChance");

var capital = userbal.text();
var statusa = userbal.next();
var pl = 0;
var txtBet = $("#txtAmount");
var btnBet = $("#btnRollIcon");
var stillBetting = false;
var actualSafeStreak = safeStreak;
txtPayout.val(1.25);
txtChance.val(79.2);

for (var i=safeStreak;i<12;i++) {
    lastStreak[i.toString()] = 0;
}


function calcMaxBaseBet(balance, _maxLoseStreak) {
    var divisor = 0;
    //balance /= 100;
    for (var i = 0; i < (_maxLoseStreak); i++) {
        divisor += (Math.pow(5, i));
    }
    var maxBaseBet = balance / divisor ;
    if (waitForSafeStreak||maxBaseBet<1) {
        maxBaseBet = 1;
    } else {
        maxBaseBet = maxBaseBet-1;
    }
    return maxBaseBet;
}

var baseBet = calcMaxBaseBet($("#lblPlayerBalance").text(), actualSafeStreak);
if(baseBet<=10){
    alert('Starting with a BASE BET of:' + baseBet + '. Altough this will work, you may want to start with a higher balance to push this faster.');
}else{
    alert('Starting with a BASE BET of:' + baseBet + '. Enter when ready');
}
$("#txtAmount").val(baseBet);

function probabilityOfStreak(bets, streak, lastStreak) {
    var aux = bets-lastStreak;
    return Math.pow(0.208, streak)*100*aux;
}


function getSafeStreak(bets, safeStreak) {
    if (probabilityOfStreak(bets, safeStreak, lastStreak[safeStreak.toString()])<2) {
        return safeStreak;
    } else {
        return getSafeStreak(bets, safeStreak+1);
    }
}


var bot = setInterval(function() {    
    
        if ($("#RollResult div").hasClass("alert-danger")) {
            stillBetting=false;
            //on loose
            baseBet = baseBet + (baseBet * 400 /100);
            lostInARow++;
            console.log('LOST! ---> In a row: ' + lostInARow);
            if (lostInARow>1) {
                inALosingStreak = true;
            }

            $("#txtAmount").val(baseBet);
        } else if ($("#RollResult div").hasClass("alert-success")){
            //On win
            stillBetting=false;
            var balance = $("#lblPlayerBalance").text();
            baseBet = calcMaxBaseBet(balance, actualSafeStreak);
            console.log("Win:" + baseBet);
            lastBalance = userbal.text();
            if (lostInARow>=safeStreak) {
                waitForSafeStreak = false;
                for (var i=lostInARow;i>=safeStreak;i--){
                    lastStreak[i.toString()] = bets;
                }
            }
            lostInARow = 0;
            inALosingStreak = false;    
            $("#txtAmount").val(baseBet)
        }else{
            stillBetting=true;
        }
    
    if(!stillBetting || bets <= 0){
        bets++;    
        actualSafeStreak = getSafeStreak(bets, safeStreak);

        btnBet.click();
    }
}, 900);


