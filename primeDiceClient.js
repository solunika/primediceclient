
var request = require('request');
var token = process.argv[2];
// var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE0MTk4NDAiLCJ0b2tlbiI6IjRlZDgzNGY5YzQ2MDRkN2I2NmZmNGYxN2QxNTAxNjdlIn0.2gs-uK0E46gPgxFJHiujOa0_0WwUPHc7lh3Pj6k4AV0';

var baseUrl = 'https://api.primedice.com/api/bet?access_token=';
var userInfo = 'https://api.primedice.com/api/users/1?access_token=';
var MYBASEBET = 1;
var baseBet = MYBASEBET;
var baseComp = '<';
var baseTarget = 79.2;
var stillBetting = false;
var delayInMS = 50;
var QUIT_SIGNAL = false;
var QUIT_SIGNAL_count = 0;

var stillBettingCount = 0;
var debug = false;

var startTimer = new Date(); 
var endTimer = new Date();
var avgTimes = [0,0,0,0,0,0,0,0,0,0];
var indexAvgTimer = 0;

function log(text, force, replace) {
    if (force || debug) {
        var lastChar = !replace ? '\n' : '\r';
        if (replace) {
          process.stdout.write("                                                                                                                                                                          "+lastChar);  
        }
        process.stdout.write(text+lastChar);   
    }
}

function getUserInfo(callback) {
    request.get(userInfo + token, callback);
}

function doBet(amount, condition, target, onWinFunction, onLoseFunction, onErrorFunction) {
    var betObject = { form: { amount: amount, condition: condition, target: target } };
    if (!stillBetting) {
        stillBettingCount = 0
        log("Apostando:" + amount);
        stillBetting = true;

        startTimer = new Date();
        request.post(
            baseUrl + token,
            betObject,
            function(error, response, body) {
                
                endTimer = new Date();
                if (!error && response.statusCode == 200) {
                    var r = JSON.parse(body);
                    if (!debug) {
                        logUserInfo(r.user, true, true);
                    }
                    if (r.bet && r.bet.win == true) {
                        onWinFunction(r);
                    } else {
                        onLoseFunction(r);
                    }
                } else {
                    onErrorFunction(error, response.statusCode);
                }
            }
        );
    } else {
        stillBettingCount++;
        log("Aun estoy esperando que vuelva ultimo bet.");
        if (stillBettingCount > 15) {
            // wtf
            stillBetting = false;
        }
    }
}


process.on('SIGINT', function() {
    log("Ctrl + c detectado: cerrando en el proximo WIN.");
    QUIT_SIGNAL = true;
    QUIT_SIGNAL_count++;

    if (QUIT_SIGNAL_count > 2) {
        process.exit();
    }

});

var baseLoseStreak = 5;
var maxLoseStreak = baseLoseStreak;
var lostInARow = 0;


var beSafe = false;
var quitOnBalance = null;

var maxLosePercentage = 130;
var minLoseBalance = 0;
var lastLoseStreak =0;

var lastBalance = 0;
if (process.argv.length > 3) {
    quitOnBalance = process.argv[3];
}

function calcMaxBaseBet(balance, _maxLoseStreak) {
    var divisor = 0;
    balance /= 100;
    for (var i = 0; i < (_maxLoseStreak); i++) {
        divisor += (Math.pow(5, i));
    }
    var maxBaseBet = balance / divisor;

    return maxBaseBet * 100;
}

function shouldBet(balance, bet) {
    if ((balance-bet)>minLoseBalance) {
        return true;
    } else {
        return false;
    }
}

function updateMinBalance(balance, force) {
    if (force||balance>=(minLoseBalance*(maxLosePercentage/100)*(1.1))) {
        minLoseBalance = balance / (maxLosePercentage/100);
        log("********------ MinLoseBalance: "+minLoseBalance/100+" ------", true);
    }
}

//amount, condition, target, onWinFunction, onLooseFunction, onErrorFunction
var onWinFunction = function(resultObject) {
    log('WIN!' + ' profit:' + resultObject.bet.profit + ' balance:' + resultObject.user.balance);
    baseBet = calcMaxBaseBet(resultObject.user.balance, maxLoseStreak);
    lastBalance = resultObject.user.balance;
    lostInARow = 0;
    updateMinBalance(resultObject.user.balance);
    if (QUIT_SIGNAL === true || (quitOnBalance && quitOnBalance <= resultObject.user.balance / 100)) {
        if (quitOnBalance <= resultObject.user.balance / 100) {
            log("TARGET BALANCE REACHED - " + resultObject.user.balance / 100, true);
        }
        log("", true);
        log("Quit on win", true);
        process.exit();
    }
    stillBetting = false;
}
var onLoseFunction = function(resultObject) {
    baseBet = baseBet + (baseBet * 400 / 100);
    lostInARow++;
    log('LOST! ---> In a row: ' + lostInARow);
    if (beSafe&&!shouldBet(resultObject.user.balance, baseBet)) {
        lostInARow = 0;
        baseBet = calcMaxBaseBet(resultObject.user.balance, maxLoseStreak);
        log("------", true);
        log("asumiendo perdida. last balance: " + lastBalance / 100 + " newBalance: " + resultObject.user.balance / 100 + " - " + new Date(), true);
        log("------", true);
        process.exit();
    }
    stillBetting = false;
}
var onErrorFunction = function(error, statusCode) {
    if (statusCode==401) {
        log("Token Invalidado", true, false);
        process.exit();
    } else {
        log("Error: "+error+" statusCode: "+statusCode);
    }
    stillBetting = false;
}

function loop() {
    doBet(baseBet, baseComp, baseTarget, onWinFunction, onLoseFunction, onErrorFunction);
    setTimeout(function() {
        loop()
    }, delayInMS);
}

function logUserInfo(user) {
    var duration = new Date(endTimer - startTimer);
    var millisecond = duration.getTime();
    var promedio = promedioTiempo (millisecond);

    log("User: " + user.username + " Balance: " + user.balance / 100 + " BaseBet: " + baseBet / 100 + " - " + new Date() + " promedio:" + Math.round((1000 / promedio)*100)/100  + " /s lastLoseStreak: "+lastLoseStreak, true, true);
}

function  promedioTiempo(duration){
  //console.log(duration);
  avgTimes[indexAvgTimer]=duration;
  indexAvgTimer++;
  if(indexAvgTimer > avgTimes.length){
    avgTimes=0;
  }

  var sum = 0;
  for( var i = 0; i < avgTimes.length; i++ ){
      //console.log(parseInt( avgTimes[i], 10 ));
      sum += parseInt( avgTimes[i], 10 ); //don't forget to add the base
  }

  var avg = sum/avgTimes.length;
  return avg ;
}

getUserInfo(function(err, data) {
    console.log(data);
    if (err||data.body=='Unauthorized') {
        log("No empezo, hubo un error. Puede ser el token: " + err + " "+data.body, true)
    } else {
        var user = JSON.parse(data.body).user;
        updateMinBalance(user.balance);
        lastBalance = user.balance;
        baseBet = calcMaxBaseBet(user.balance, maxLoseStreak);
        log("Empezo! (Poner debug en true para ver el log)", true, false);
        logUserInfo(user, true, false);
        // loop();
    }
})










