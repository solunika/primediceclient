const _ = require ('lodash');
var request = require('request');
var async = require('async');
var dummyData = require('./data/dummyData.js');

var dummyUser = null;
var targetMoney = process.argv[2];
var safeStreak = parseInt(process.argv[3]);
var maxCalls = process.argv[4];
var token = process.argv[5];
var waitForSafeStreak = parseInt(process.argv[6]);

var baseUrl = 'https://api.primedice.com/api/bet?access_token=';
var userInfo = 'https://api.primedice.com/api/users/1?access_token=';


var QUIT_SIGNAL = false;
var QUIT_SIGNAL_count = 0;

var condition = '<';
var targetPercentage = 79.2;

var bets = 0;
var lostInARow =0;
var perdidasAsumidas = 0;
var originalBalance = null;
var QUIT_SIGNAL = false;
var QUIT_SIGNAL_count = 0;
var baseBet = 0;
var debug = false;
var dummyBet = 10;
var lastStreak = {};
var actualSafeStreak = safeStreak;

var initialBalance = 0;

var startTimer = new Date();
var endTimer = new Date();
var lastUser = null;
var usdExchangeRate = 413.03 / 1000000;

for (var i=safeStreak;i<12;i++) {
    lastStreak[i.toString()] = 0;
}

function getUserInfo(callback) {
    request.get(userInfo + token, function(err, data) {
        if (err||data.body=='Unauthorized') {
            quit("No empezo, hubo un error. Puede ser el token: " + err + " "+data.body, true);
        } else {
            callback(JSON.parse(data.body).user);
        }
    });
}

var lastWasReplace = false;
function log(text, force, replace) {
    if (force || debug) {
        var lastChar = !replace ? '\n' : '\r';
        if (replace) {
          process.stdout.write("                                                                                                                                                                                                                   "+lastChar);  
        } else if (lastWasReplace) {
            process.stdout.write(lastChar);
        }
        process.stdout.write(text+" | "+new Date()+lastChar);
        lastWasReplace = replace;
    }
}

function logUserInfo(user) {
    lastUser = user;
    var betsText = String("         " + bets).slice(-9); // returns 00123
    var baseBetText = String("         " + (Math.round(baseBet)/100).toFixed(2)).slice(-9); // returns 00123
    var profitInUsd = ((user.balance-initialBalance)/100*usdExchangeRate).toFixed(2);
    log("User: "+user.username + " Balance: " + (Math.round(user.balance)/100).toFixed(2) + " Bets: " + betsText + " BaseBet: " + baseBetText + " Total Profit in USD: " + profitInUsd +  " ActualSafeStreak: "+actualSafeStreak+" ProbabilityOfStreak: "+Math.round(probabilityOfStreak(bets, actualSafeStreak, lastStreak[actualSafeStreak.toString()])*100)/100, true, true);
}

function calcMaxBaseBet(balance, _maxLoseStreak) {
    var divisor = 0;
    balance /= 100;
    for (var i = 0; i < (_maxLoseStreak); i++) {
        divisor += (Math.pow(5, i));
    }
    var maxBaseBet = balance / divisor * 100;
    if (maxBaseBet<1) {
        maxBaseBet = 1;
    } else {
        maxBaseBet = maxBaseBet-1;
    }
    return maxBaseBet;
}

var onWinFunction = function(user) {
    log('WIN! ----> balance:' + user.balance);
    baseBet = calcMaxBaseBet(user.balance, actualSafeStreak);
    lastBalance = user.balance;
    if (lostInARow>=safeStreak) {
        waitForSafeStreak = 0;
        log("**** BUMPING BETS *****", true, false);
        for (var i=lostInARow;i>=safeStreak;i--){
            lastStreak[i.toString()] = bets;
            log("--> LostInARow: "+i, true, false);
        }
    }
    lostInARow = 0;
    inALosingStreak = false;
    if (QUIT_SIGNAL === true || (targetMoney && targetMoney <= user.balance / 100)) {
        var message = "";
        if (targetMoney <= user.balance / 100) {
            message = "TARGET BALANCE REACHED - " + user.balance / 100;
        }
        quit(message);
    }
}
var onLoseFunction = function(user) {
    baseBet = baseBet + (baseBet * 400 / 100);
    lostInARow++;
    log('LOST! ---> In a row: ' + lostInARow);
    if (lostInARow>1) {
        inALosingStreak = true;
    }
}

var onErrorFunction = function(error, response) {
    if (response&&response.statusCode==401) {
        quit("Token Invalidado");
    } else {
        log("Error: "+error+" response: "+JSON.stringify(response), false, false);
        bets--;
    }
}

var funcs = [
    {probability: 79.2, fn: function(user, amount){
            user.balance=user.balance + (amount * 0.25);
            onWinFunction(user);
        }
    }, // win
    {probability: 20.8, fn: function(user, amount){
            user.balance=user.balance - amount;
            onLoseFunction(user);
        }
    }, // lose
];

process.on('SIGINT', function() {
    log("Ctrl + c detectado: cerrando en el proximo WIN.", true, false);
    QUIT_SIGNAL = true;
    QUIT_SIGNAL_count++;

    if (QUIT_SIGNAL_count > 2) {
        quit("Cerrado por Ctrl + c");
    }

});

function quit (message) {
    log(message, true, false);
    logUserInfo(lastUser, true, false);
    var profit = (lastUser.balance - initialBalance)/100;
    log("Profit: "+ profit +" lastStreak: "+JSON.stringify(lastStreak), true, false);
    process.exit();
}

function getSafeStreak(bets, safeStreak) {
    if (probabilityOfStreak(bets, safeStreak, lastStreak[safeStreak.toString()])<2) {
        return safeStreak;
    } else {
        return getSafeStreak(bets, safeStreak+1);
    }
}

function realBet(next) {
    var betObject = { form: { amount: baseBet, condition: condition, target: targetPercentage } };
    startTimer = new Date();
    request.post(baseUrl + token, betObject, function(error, response, body) {
        endTimer = new Date();
        if (!error && response.statusCode == 200) {
            var r = JSON.parse(body);
            if (!debug) {
                logUserInfo(r.user, true);
            }
            if (r.bet && r.bet.win == true) {
                onWinFunction(r.user);
            } else {
                onLoseFunction(r.user);
            }
        } else {
            onErrorFunction(error, response);
        }
        next();
    })
}

function chanceBet(next) {
    if (dummyUser.balance-baseBet<=100) {
        quit("No more money. lostInARow: "+lostInARow+" ");
    } else {
        logUserInfo(dummyUser);
    }
    var total = 0;
    var n = Math.random();
    for(var i=0; i<funcs.length; ++i) {
        total+=funcs[i].probability;
        if(n*100<total) {
            funcs[i].fn(dummyUser, baseBet)
            break;
        }
    }
    next();
}


function probabilityOfStreak(bets, streak, lastStreak) {
    var aux = bets-lastStreak;
    return Math.pow(0.208, streak)*100*aux;
}

var isChanceBet = !token;
var betFunction = isChanceBet?chanceBet:realBet;
var userInfoFunction = isChanceBet?dummyData.getUserInfo: getUserInfo;

function bet(next) {
    bets++;
    if (bets>maxCalls) {
        quit("MAX CALLS REACHED");
    }
    actualSafeStreak = getSafeStreak(bets, safeStreak+waitForSafeStreak);
    betFunction(next);
}

function start (callback) {
    userInfoFunction(function (user) {
        actualSafeStreak = getSafeStreak(bets, safeStreak+waitForSafeStreak);
        baseBet = calcMaxBaseBet(user.balance, actualSafeStreak);
        initialBalance = user.balance;
        if (isChanceBet) {
            log("****** CHANCE BET ******", true, false);
            dummyUser = user;
        } else {
            log("****** REAL BET ******", true, false);
        }
        logUserInfo(user);
        callback();
    });
}
start(function() {
    async.forever(bet, function(err) {
        log(err, true, false);
    })
})
