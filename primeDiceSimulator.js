var perdidasAsumidas = 0;
var originalBalance = null;

var request = require('request');
var betEngine = require('./simulatorEngine.js');

var clusterCallback = null;
var token = process.argv[2];
// var token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE0MTk4NDAiLCJ0b2tlbiI6IjRlZDgzNGY5YzQ2MDRkN2I2NmZmNGYxN2QxNTAxNjdlIn0.2gs-uK0E46gPgxFJHiujOa0_0WwUPHc7lh3Pj6k4AV0';

var baseUrl = 'https://api.primedice.com/api/bet?access_token=';
var userInfo = 'https://api.primedice.com/api/users/1?access_token=';
var MYBASEBET = 1;
var baseBet = MYBASEBET;
var baseComp = '<';
var baseTarget = 79.2;

var delayInMS = 1;
var QUIT_SIGNAL = false;
var QUIT_SIGNAL_count = 0;

var stillBettingCount = 0;
var debug = true;
var showInfo = true;

var RISKMANAGEMENTPERCENTAGE = 30;

var lostInARowHelper = require('./lostInARowData.js');

var baseLoseStreak = 7;
var maxLoseStreak = baseLoseStreak;
var lostInARow = 0;

var quitOnBalance = null;

var maxLosePercentage = 130;
var minLoseBalance = 0;

var lastBalance = 0;

var bets = 0;



function log(text, force, replace) {
    if ((force || debug)&&showInfo) {
        var lastChar = !replace ? '\n' : '\r';
        // if (replace) {
        //   process.stdout.write("                                                                                                                                                                          "+lastChar);  
        // }
        // process.stdout.write(text+lastChar);
    }
}



process.on('SIGINT', function() {
    log("Ctrl + c detectado: cerrando en el proximo WIN.");
    QUIT_SIGNAL = true;
    QUIT_SIGNAL_count++;

    if (QUIT_SIGNAL_count > 2) {
        process.exit();
    }

});


if (process.argv.length > 3) {
    quitOnBalance = process.argv[3];
}

function calcMaxBaseBet(balance, _maxLoseStreak) {
    var divisor = 0;
    balance /= 100;
    for (var i = 0; i < (_maxLoseStreak); i++) {
        divisor += (Math.pow(5, i));
    }
    var maxBaseBet = balance / divisor;

    return maxBaseBet * 100;
}

function shouldBet(balance, bet) {
    if ((balance-bet)>minLoseBalance && balance >= 0) {
        return true;
    } else {
        return false;
    }
}

function updateMinBalance(balance, force) {
    if (force||balance>=(minLoseBalance*(maxLosePercentage/100)*(1.1))) {
        minLoseBalance = balance / (maxLosePercentage/100);
        log("********------ MinLoseBalance: "+minLoseBalance/100+" ------", true);
    }
}


//amount, condition, target, onWinFunction, onLooseFunction, onErrorFunction
var onWinFunction = function(resultObject) {
    log('WIN!' + ' profit:' + resultObject.bet.profit + ' balance:' + resultObject.user.balance);
    baseBet = calcMaxBaseBet(resultObject.user.balance, maxLoseStreak);
    lastBalance = resultObject.user.balance;
    lostInARow = 0;
    updateMinBalance(resultObject.user.balance);
    if (QUIT_SIGNAL === true || (quitOnBalance && quitOnBalance <= resultObject.user.balance / 100)) {
        if (quitOnBalance <= resultObject.user.balance / 100) {
            log("TARGET BALANCE REACHED - " + resultObject.user.balance / 100, true);
        }
        clusterCallback(resultObject.user.balance);
        process.exit("GOOD");
    }
    betEngine.setStillBetting = false;
};


var onLoseFunction = function(resultObject) {
    baseBet = baseBet + (baseBet * 400 / 100);
    lostInARow++;
    console.log('LOST! ---> In a row: ' + lostInARow);
    
    lostInARowHelper.addLostInARowData(lostInARow, bets);        
    
     if (!shouldBet(resultObject.user.balance, baseBet) && lostInARowHelper.getProbailityOfLoosingForStreak(lostInARow)<= RISKMANAGEMENTPERCENTAGE) {
         console.log("lost in a row "+lostInARow+" basebet: "+baseBet);
         lostInARow = 0;
         baseBet = calcMaxBaseBet(resultObject.user.balance, maxLoseStreak);
         log("------", true);
         console.log("asumiendo perdida. last balance: " + lastBalance / 100 + " newBalance: " + resultObject.user.balance / 100 + " - " + new Date(), true);
         log("------", true);
         if (perdidasAsumidas=>3) {
             clusterCallback(resultObject.user.balance);
             process.exit("BAD");
         } else {
             updateMinBalance(resultObject.user.balance, true);
         }
         perdidasAsumidas++;
     }
    betEngine.setStillBetting = false;
    
//     if (lostInARow > maxLoseStreak - 1  && lostInARowHelper.getProbailityOfLoosingForStreak(lostInARow)<= RISKMANAGEMENTPERCENTAGE ) {
//         lostInARow = 0;
//         baseBet = calcMaxBaseBet(resultObject.user.balance, maxLoseStreak);
//         log("------", true);
//         log("asumiendo perdida. last balance: " + lastBalance / 100 + " newBalance: " + resultObject.user.balance / 100 + " - " + new Date(), true);
//         log("------", true);
//         process.exit();
//     }
}
var onErrorFunction = function(error, statusCode) {
    if (statusCode==401) {
        log("Token Invalidado", true, false);
        process.exit();
    } else {
        log("Error: "+error+" statusCode: "+statusCode);
    }
}

function loop() {
    betEngine.doBet(baseBet, baseComp, baseTarget, onWinFunction, onLoseFunction, onErrorFunction);
    setTimeout(function() {
        bets++;
        loop();
    }, delayInMS);
}





function run (_token, target, callback) {
    clusterCallback = callback;
    token = _token;
    quitOnBalance = target;
    betEngine.getUserInfo(function(err, data) {
        originalBalance = data.user.balance;  
        updateMinBalance(data.user.balance);
        baseBet = calcMaxBaseBet(data.user.balance, maxLoseStreak);
        log("Empezo! (Poner debug en true para ver el log)", true, false);
        betEngine.logUserInfo(data.user, true, false);
        loop();
    
});
}

if (process.argv.length>4&&process.argv[4]=='run') {
    // console.log('run?');
    showInfo = true;
    run(token, quitOnBalance, function(data) {
        console.log(data/100);
    })
}
module.exports.run = run;









