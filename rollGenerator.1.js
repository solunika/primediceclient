var crypto = require('crypto');
var randomstring = require("randomstring");

var maxCalls = process.argv[2];
var clientSeed = process.argv[4] || "111111111111111111111111111111";


var roll = function(key, text) {
    //create HMAC using server seed as key and client seed as message
    var hash = crypto.createHmac('sha512', key).update(text).digest('hex');

    var index = 0;

    var lucky = parseInt(hash.substring(index * 5, index * 5 + 5), 16);

    //keep grabbing characters from the hash while greater than 
    while (lucky >= Math.pow(10, 6)) {
        index++;
        lucky = parseInt(hash.substring(index * 5, index * 5 + 5), 16);

        //if we reach the end of the hash, just default to highest number
        if (index * 5 + 5 > 128) {
            lucky = 99.99;
            break;
        }
    }

    lucky %= Math.pow(10, 4);
    lucky /= Math.pow(10, 2);

    return lucky;
}

function findHash() {
    var serverSeed = randomstring.generate({
        length: 64,
        charset: 'abcdefghijklmnopqrstuvwxyz1234567890'
    })
    var rol = roll(serverSeed, clientSeed+'-'+"0");
    if (rol == 50.04){
        rol = roll(serverSeed, clientSeed+'-'+"1");
        if (rol == 82.11) {
            return serverSeed;
        } else return undefined;
        
    } else {
        return undefined;
    }
}

for (var i =0;i<maxCalls;i++) {
    var hash = findHash();
    if(hash) {
        console.log('LOTERIA! '+hash);
        break;
    }
}
