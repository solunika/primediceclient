const _ = require ('lodash');
var request = require('request');
var async = require('async');
var dummyData = require('./data/dummyData.js');
var crypto = require('crypto');

var maxCalls = process.argv[2];
var serverSeed = process.argv[3] || "4229e39732c653c058d30df12b0971b8cb7b5101c67432309c7e7048436ed5cc";
var clientSeed = process.argv[4] || "2d4912365c5e089aa6865794253de15b";


var condition = '<';
var targetPercentage = 79.2;

var bets = 0;
var lostInARow =0;

var lastStreak = {};

var startTimer = new Date();
var endTimer = new Date();

for (var i=0;i<16;i++) {
    lastStreak[i.toString()] = 0;
}

function quit () {
    process.exit();
}

var roll = function(key, text) {
    //create HMAC using server seed as key and client seed as message
    var hash = crypto.createHmac('sha512', key).update(text).digest('hex');

    var index = 0;

    var lucky = parseInt(hash.substring(index * 5, index * 5 + 5), 16);

    //keep grabbing characters from the hash while greater than 
    while (lucky >= Math.pow(10, 6)) {
        index++;
        lucky = parseInt(hash.substring(index * 5, index * 5 + 5), 16);

        //if we reach the end of the hash, just default to highest number
        if (index * 5 + 5 > 128) {
            lucky = 99.99;
            break;
        }
    }

    lucky %= Math.pow(10, 4);
    lucky /= Math.pow(10, 2);

    return lucky;
}
var lostInARows = {};

function chanceBet() {
    var total = 0;
    var n = roll(serverSeed, clientSeed+"-"+bets);
    if (n<targetPercentage) {
        if (lostInARow>5) {
            for (var i = lostInARow;i>5;i--) {
                if (!lostInARows[i.toString()]) {
                    lostInARows[i.toString()] = {}
                    lostInARows[i.toString()].bets = [];
                    lostInARows[i.toString()].repeat = 0;
                }
                lostInARows[i.toString()].bets.push(bets);
                lostInARows[i.toString()].repeat++;
            }
        }
        lostInARow = 0;
    } else {
        lostInARow++;
    }
}
function probabilityOfStreak(bets, streak, lastStreak) {
    var aux = bets-lastStreak;
    return Math.pow(0.208, streak)*100*aux;
}

function bet() {
    chanceBet();
    bets++;
}
for (var i = 0;i<maxCalls;i++) {
    bet();
}
console.log(lostInARows);
