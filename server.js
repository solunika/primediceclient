var sys = require('sys')
var spawn = require('child_process').spawn;
var filename ="./nohup.log";

var tail = spawn("tail", ["-f", filename]);

http = require('http');
http.createServer(function (req, res) {
  console.log("new connection..");
  res.writeHead(200, {'Content-Type': "text/plain;charset=UTF-8"});
  tail.stdout.on("data", function (data) {
    res.write(data);
  }); 
}).listen(81);
