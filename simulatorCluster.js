const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const primeDiceClient = require('./primeDiceClientReloaded.js');


var quitOnBalance = process.argv[3];
var token = process.argv[2];

function fork() {
    var worker = cluster.fork(counts);
        // worker.on('message', function (msg) {
        //     console.log(msg);
        //     counts.countGoods += msg.good;
        //     counts.countBads += msg.bad;
        //     // console.log("GOOD: "+counts.countGoods+" BAD:"+counts.countBads);
        // })
}
if (cluster.isMaster) {
    var repeat = 20;
    var count = 0;
    var counts = {
        countGoods: 0,
        countBads: 0
    }
    for(var i=0;i<1;i++) {
        fork(counts);
    }
    cluster.on('exit', function(worker, code, signal) {
        count++;
        if (repeat>count) {
            fork(counts);
        }
    });
} else {
    primeDiceClient.run(token, quitOnBalance, function(data) {
        if ((data/100)>=quitOnBalance) {
            console.log("**** GOOD : "+data/100);
            // process.send({good: 1});
        } else {
            console.log("**** BAD : "+data/100);
            // process.send({bad: 1});
        }
    });
}