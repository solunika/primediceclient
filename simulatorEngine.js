var stillBetting = false;
var startTimer = new Date(); 
var endTimer = new Date();
var avgTimes = [0,0,0,0,0,0,0,0,0,0];
var indexAvgTimer = 0;
var betAmount = 0;

var dummyResponse = {
    "bet": {
        "id": 9866821600,
        "player": "Gabenitez",
        "player_id": "1426964",
        "amount": 248,
        "target": 79.2,
        "profit": 62,
        "win": true,
        "condition": "<",
        "roll": 6.72,
        "nonce": 15151,
        "client": "b7042e41e5e32eb27f061d81aa7146b6",
        "multiplier": 1.25,
        "timestamp": "2016-03-25T20:47:09.031Z",
        "jackpot": false,
        "server": "268c2d1e03c26a7c899f7975a56ec57b0d928b8d64b82aa6646032fe34e98fca",
        "revealed": false
    },
    "user": {
        "id": 1,
        "userid": "1426900",
        "username": "Gabenitez",
        "balance": 14000000.0000,
        "password": true,
        "address": "1Lwg8khAHDeEadRy1obgKxuGYTysMMYqie",
        "registered": "2016-03-24T18:51:38.101Z",
        "otp_enabled": false,
        "email_enabled": false,
        "address_enabled": false,
        "wagered": 9244472,
        "profit": -806990.0169,
        "bets": 15152,
        "wins": 12046,
        "losses": 3106,
        "win_risk": 25016645,
        "lose_risk": 24520655,
        "messages": 0,
        "referred": 0,
        "affiliate_total": 0,
        "nonce": 15152,
        "client": "b7042e41e5e32eb27f061d81aa7146b6",
        "previous_server": null,
        "previous_client": null,
        "previous_server_hashed": null,
        "next_seed": "5d34b09a6d8a04ced7761abb89f448f97687151db49a4a033dac173122533374",
        "server": "268c2d1e03c26a7c899f7975a56ec57b0d928b8d64b82aa6646032fe34e98fca"
    }};


var getUserInfo = function (callback) {
    callback(undefined, dummyResponse);
}

function  promedioTiempo(duration){
  //console.log(duration);
  avgTimes[indexAvgTimer]=duration;
  indexAvgTimer++;
  if(indexAvgTimer > avgTimes.length){
    avgTimes=0;
  }

  var sum = 0;
  for( var i = 0; i < avgTimes.length; i++ ){
      //console.log(parseInt( avgTimes[i], 10 ));
      sum += parseInt( avgTimes[i], 10 ); //don't forget to add the base
  }

  var avg = sum/avgTimes.length;
  return avg ;
}

function logUserInfo(user) {
    var duration = new Date(endTimer - startTimer);
    var millisecond = duration.getTime();
   
    var promedio = promedioTiempo (millisecond);

    console.log("User: " + user.username + " Balance: " + Math.round((user.balance / 100)) + " BaseBet: " + Math.round(betAmount ) /100 + " - " + new Date() + " promedio:" + Math.round((1000 / promedio)*100)/100  + " /s", true, true);
}



var doBet = function (amount, condition, target, onWinFunction, onLoseFunction, onErrorFunction) {      
betAmount = amount;
if (!stillBetting) {        
        stillBettingCount = 0;    
        startTimer = new Date();
        var funcs = [
          {probability: 79.2, fn: function(){            
                    endTimer = new Date();
                    dummyResponse.user.balance=dummyResponse.user.balance + (amount * 0.25);                                
                    logUserInfo(dummyResponse.user, true, false);
                    onWinFunction(dummyResponse);  
                }
            }, // win
          {probability: 20.8, fn: function(){                 
                    endTimer = new Date();
                    dummyResponse.user.balance=dummyResponse.user.balance - amount;
                    logUserInfo(dummyResponse.user, true, false);
                    onLoseFunction(dummyResponse);  
                }
            }, // lose
        ];

        var total = 0;
        var n = Math.random();
        for(var i=0; i<funcs.length; ++i) {
          total+=funcs[i].probability;
          if(n * 100<total) {
            funcs[i].fn.call(this);
            break;
          }
        }
}else {
        stillBettingCount++;
        log("Aun estoy esperando que vuelva ultimo bet.");
        if (stillBettingCount > 15) {
            // wtf
            stillBetting = false;
        }
    }
    
}


var setStillBetting = function (status){
	stillBetting = status;
}  


module.exports.setStillBetting = setStillBetting;
module.exports.getUserInfo = getUserInfo;
module.exports.doBet = doBet;
module.exports.logUserInfo=logUserInfo;